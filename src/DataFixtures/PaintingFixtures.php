<?php

namespace App\DataFixtures;

use App\Entity\Painting;
use Cocur\Slugify\Slugify;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker;

class PaintingFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Faker\Factory::create('fr_FR');
        $slugify = new Slugify();
        for ($i = 1; $i <= 25; $i++) {
            $paint = new Painting();
            $title = $faker->words($faker->numberBetween(3, 5), true);
            $paint->setTitle($title)
                ->setDescription($faker->paragraphs(3, true))
                ->setCreatedAt(new \DateTime(date_format($faker->dateTimeBetween('-30 days', 'now'), "Y/m/d H:i:s")))
                ->setImage($i . '.jpg')
                ->setHeight($faker->numberBetween(20, 60))
                ->setWidth($faker->numberBetween(30, 60))
                ->setSlug($slugify->slugify($title));

            $manager->persist($paint);
            $manager->flush();
        }
    }
}
